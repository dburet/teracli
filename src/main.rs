#[macro_use]
extern crate clap;

use anyhow::{Context, Result};
use clap::App;
use serde_json::Map;
use serde_json::Value;
use tera::{Context as Tera_context, Tera};

fn main() -> Result<()> {
    // parse command line argument with clap 2.33
    let yml = load_yaml!("clap.yml");
    let arg_parser = App::from_yaml(yml).get_matches();

    // unwrap is safe on "required" arguments thanks to claps
    let template_file = arg_parser.value_of("TEMPLATE_FILE").unwrap();
    let values_file = arg_parser.value_of("CONTEXT_FILE").unwrap();

    // set context file format, default is toml
    let format = arg_parser.value_of("FORMAT").unwrap_or("toml");

    //load context files into a String
    let raw_values = std::fs::read_to_string(values_file).
        with_context(|| format!("Failed to read value file {}", values_file))?;

    // parse values from context file
    let values: Map<String, Value> = match format {
        "json" => serde_json::from_str(&raw_values)
            .with_context(|| format!("I do not understand your JSON context file: {}", values_file))?,
        "yaml" => serde_yaml::from_str(&raw_values)
            .with_context(|| format!("I do not understand your YAML context file:  {}", values_file))?,
        "toml" => toml::from_str(&raw_values)
            .with_context(|| format!("I do not understand your TOML context file:  {}", values_file))?,
        _ => anyhow::bail!("You specified an unsupported format on the command line: {}",format)
    };

    let mut context = Tera_context::from_serialize(values)
        .with_context(|| "Tera Failed to add values to its context")?;

    //add context from args
    if let Some(arg_context) = arg_parser.values_of("ADD_CONTEXT") {
        for pair in arg_context {
            match pair.find("=") {
                Some(i) => context.insert(&pair[..i], &pair[i + 1..]),
                None => anyhow::bail!("bad -a argument. {:?}. expected format is: key=value",pair)
            };
        };
    };

    // create tera instance
    let mut tera = Tera::default();
    tera.autoescape_on(vec![]);

    //some macros to load?
    if let Some(macro_file) = arg_parser.value_of("MACRO_FILE") {
        let macro_template = std::fs::read_to_string(macro_file)
            .with_context(|| format!("Failed to read macro file {}", macro_file))?;
        tera.add_raw_template("macros.tera", &macro_template)
            .with_context(|| format!("Tera engine failed load your macros"))?;
    }

    //read template file
    let main_template = std::fs::read_to_string(template_file)
        .with_context(|| format!("Failed to read template file {}", template_file))?;

    // improve Tera errors message: template_name = template filename (without path)
    let template_name = match &template_file.rfind("/") {
        Some(x) => &template_file[x+1..],
        None => template_file
    };
    tera.add_raw_template(template_name, &main_template)
        .with_context(|| format!("Tera engine failed load your template"))?;

    //render with Tera
    println!("{}", tera.render(template_name, &context)
        .with_context(|| format!("Tera engine failed to render your template"))?
    );

    Ok(())
}