#!/bin/bash

echo ">>> toml"
cargo run -- tests/01*/*toml tests/01-*/basic.tera

echo ">>> yaml"
cargo run -- -f yaml tests/01*/*yaml tests/01-*/basic.tera

echo ">>> json"
cargo run -- -f json tests/01*/*json tests/01-*/basic.tera

echo ">>> toml, macros, -a"
cargo run -- -m macros.tera tests/06*/*toml tests/06-*/template.tera -a env=dev